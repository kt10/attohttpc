use ruc::*;
use std::{thread, time::Duration};

// const QA01: &str = "https://dev-qa01.dev.findora.org:8668";
const QA02: &str = "https://dev-qa02.dev.findora.org:8668";
const STAGING: &str = "https://dev-staging.dev.findora.org:8668";

fn main() {
    exec_qa2();
    exec_staging();
    thread::sleep(Duration::from_secs(10000000000000));
}

fn exec_qa2() {
    (0..100).for_each(|_| {
        thread::spawn(|| req(QA02));
    });
}

fn exec_staging() {
    (0..100).for_each(|_| {
        thread::spawn(|| req(STAGING));
    });
}

fn req(path: &str) {
    let proxy = attohttpc::ProxySettings::from_env();

    loop {
        ruc::info_omit!(attohttpc::get(format!("{}/blocks_since/0", path))
            .connect_timeout(Duration::from_secs(10))
            .proxy_settings(proxy.clone())
            .send());
        ruc::info_omit!(attohttpc::get(format!("{}/block_log", path))
            .connect_timeout(Duration::from_secs(10))
            .proxy_settings(proxy.clone())
            .send());
    }
}
